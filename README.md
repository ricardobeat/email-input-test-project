## How to run

The code should run as-is in all the evergreen browsers, without a compile step. Use `npx serve` to start a local server then open <http://localhost:5000>.

To compile scripts for legacy browser compatibility, run using `npx parcel index.html` instead.

Live demo: https://email-input-test-project.ricardobeat.now.sh/

Parcel build: https://email-input-test-project.ricardobeat.now.sh/build/

## Footnotes

- Accessibility: outlines are removed following what I saw on Miro.com. We could get the best of both worlds by detecting keyboard users and re-enabling focus rings dynamically

- there are some minor quirks in spacing I'd love to fix - like how, only for the first entered e-mail, the input text doesn't stay in place once rendered due to left padding

- behaviour for the backspace and space keys is not defined in the spec. I made backspace delete each previous token, and space start a new entry as is common for this type of tokenized input

- to be fully self-contained, the component styles are in the JS source and are also rendered at runtime. This could be achieved through CSS imports or other mechanisms if developing with a build process in mind

- no SSR support; you'll notice the missing input on page load. CSS could be extracted from components in a similar fashion to `emotion`. Rendering is via DOM API only, could be generated with JSDom but preferrably use a view library that can render to strings for the server


