import EmailInput from "./src/email-input.js";
import { email } from "./src/lib/random.js";

const emailInput = EmailInput.mount(document.querySelector("#email-input"));

// const { addEmail, getEmails, getValidEmails, getCount, focus } = emailInput

document.querySelector("#add-email").addEventListener("click", () => emailInput.addEmail(email()), false);
document.querySelector("#get-count").addEventListener("click", () => alert(emailInput.getCount()), false);

emailInput.subscribe((state) => {
  console.log(state);
});

setTimeout(() => emailInput.focus(), 10); // focus input on page load
