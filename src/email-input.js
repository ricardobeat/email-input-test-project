import { Component } from "./lib/component.js";
import theme from "./theme.js";

/** Most lax e-mail validation possible: @ in the middle, at least one dot on the right */
const isValidEmail = (email) => /^.+@.+\.(.*\.?)+$/.test(email);

/** Todo: proper parsing for CSV would deal with quoted values, escaped chars etc */
const extractEmailsFromInput = (input) => input.replace(/\s/g, "").split(/[,;]/g);

const KEYCODES = {
  ENTER: 13,
  SPACE: 32,
  COMMA: 188,
  BACKSPACE: 8,
};

const IconX = `<svg viewBox="0 0 24 24" width="8" height="8">
    <line x1="1" y1="1" x2="23" y2="23" stroke="currentColor" stroke-width="5" stroke-linecap="round"></line>
    <line x1="23" y1="1" x2="1" y2="23" stroke="currentColor" stroke-width="5" stroke-linecap="round"></line>
  </svg>`;

/**
 * The main EmailInput component. The list of emails is mirrored to the DOM and
 * changes manually synced. See notes on the limitations of this approach.
 */
class EmailInput extends Component {
  constructor(root) {
    super(root);

    this.emails = this.root.getElementsByClassName("email-input--item"); // this is a live NodeList
    this.state = { emails: [] };

    /* 2011 called and wants it's Backbone back */
    this.listen(this.root, "email-removed", this.onRemove);
    this.listen(this.root, "click", this.captureFocus);
    this.listen(this.root, "focus", this.captureFocus);
    this.listen(this.refs.input, "keydown", this.onKeydown);
    this.listen(this.refs.input, "keyup", this.growInput);
    this.listen(this.refs.input, "paste", this.onPaste);
    this.listen(this.refs.input, "blur", this.consumeInput);

    this.subscribers = [];
  }

  subscribe(fn) {
    this.subscribers.push(fn);
    const unsubscribe = () => this.subscribers.splice(this.subscribers.indexOf(fn), 1);
    return unsubscribe;
  }

  /** Takes the current input value and adds it to the list, then clears the input */
  consumeInput() {
    const { input } = this.refs;
    const value = this.currentInputValue();
    if (value) {
      this.addEmail(value);
      input.value = "";
    }
  }

  /** Redirect focus to the input when the root container is clicked */
  captureFocus(e) {
    if (e.target === this.root || e.target === this.refs.content) {
      this.refs.input.focus();
    }
  }

  /** Handle incoming pasted text on the input */
  onPaste(e) {
    const text = e.clipboardData.getData("text");
    if (text.indexOf("@") >= 0) {
      extractEmailsFromInput(text).forEach((value) => this.addEmail(value));
      e.preventDefault();
    }
  }

  /**
   * Special control characters handling on the input, trigger inserts.
   *
   * If a modifier key is pressed, the user is not typing one of the control characters.
   * This allows use of the < character on the comma key for example, or entering emoji via ctrl+cmd+space on a Mac.
   */
  onKeydown(e) {
    if (e.ctrlKey || e.metaKey || e.shiftKey) return;

    switch (e.which) {
      case KEYCODES.COMMA:
      case KEYCODES.ENTER:
      case KEYCODES.SPACE:
        this.consumeInput();
        e.preventDefault();
        break;

      case KEYCODES.BACKSPACE:
        if (this.currentInputValue().length === 0) {
          this.removeLastEmail();
          e.preventDefault();
        }
        break;

      case KEYCODES.SPACE:
        e.preventDefault();
    }
  }

  /**
   * Mirror the input's content into another element to calculate length.
   * The same could be achieved without code by using contendeditable=true,
   * but then you'd have to replicate the input placeholder and accessibility features.
   */
  growInput() {
    const { input, ghostInput } = this.refs;
    const minWidth = this._minWidth || (this._minWidth = input.getBoundingClientRect().width);
    ghostInput.innerHTML = input.value;
    const width = Math.ceil(ghostInput.getBoundingClientRect().width);
    input.style.width = Math.ceil(Math.max(width, minWidth) + 18) + "px";
  }

  currentInputValue() {
    return this.refs.input.value.trim();
  }

  onRemove(e) {
    const { value } = e.detail;
    this.removeEmail(value);
  }

  update() {
    this.subscribers.forEach((fn) => fn(this.state));
  }

  addEmail(value) {
    const { items, content } = this.refs;
    if (!this.state.emails.includes(value)) {
      this.state.emails.push(value);
      items.appendChild(new EmailInputItem(null, { value }).root);
      content.scrollTop = content.scrollHeight;
      this.update();
    }
  }

  removeEmail(email) {
    const index = this.state.emails.indexOf(email);
    this.state.emails.splice(index, 1);
    this.refs.items.children.item(index).remove();
    this.update();
  }

  removeLastEmail() {
    this.state.emails.pop();
    const lastElement = this.refs.items.lastElementChild;
    if (lastElement) {
      lastElement.remove();
      this.update();
    }
  }

  get api() {
    return {
      focus: () => this.refs.input.focus(),
      addEmail: (email) => this.addEmail(email),
      removeEmail: (email) => this.removeEmail(email),
      getEmails: () => this.state.emails,
      getValidEmails: () => this.state.emails.filter(isValidEmail),
      getCount: () => this.api.getValidEmails().length,
      subscribe: (fn) => this.subscribe(fn),
    };
  }

  get template() {
    return /*html*/ `
      <div class="email-input">
        <div class="email-input-container" data-ref="content">
          <div class="email-input-items" data-ref="items"></div>
          <input type="text" class="email-input-field" data-ref="input" placeholder="add more people..." />
          <span class="email-input-field email-input-field--ghost" data-ref="ghostInput" />
        </div>
      </div>
    `;
  }

  get styles() {
    return /*css*/ `
      .email-input {
        box-sizing: border-box;
        background: ${theme.color.backgroundWhite};
        color: ${theme.color.text};
        width: 100%;
        height: 16ex;
        border-radius: ${theme.radius.normal};
        border: 1px solid ${theme.color.muted};
        overflow: hidden;
      }

      /* this extra wrapper is necessary to avoid the scrollbar clipping the rounded corners
      in Chrome & Safari for Mac */
      .email-input-container {
        box-sizing: border-box;
        height: 100%;
        padding: ${theme.padding.normal};
        overflow-y: auto;
      }

      .email-input-items {
        display: inline;
        vertical-align: top;
        line-height: 1;
      }

      /* not in the spec but might be desirable?
      .email-input:focus-within {
        border-color: ${theme.color.action};
      }
      */

      .email-input-field {
        border: 0;
        display: inline-block;
        margin: 0 ${theme.padding.small} ${theme.padding.small} 0;
        padding: ${theme.padding.small};
        font-size: 1rem;
        line-height: 16px;
        outline: 0;
        min-width: 0;
        max-width: calc(100% - ${theme.padding.normal});
      }

      .email-input-field--ghost {
        visibility: hidden;
        position: absolute;
      }

      .email-input-field::placeholder {
        color: ${theme.color.muted}
      }
    `;
  }
}

/**
 * Component for a single tokenized email value
 *
 * Doesn't do much, just encapsulates it's own styles, renders current value
 * and triggers the remove events which are captured by the parent.
 *
 */
class EmailInputItem extends Component {
  constructor(root, data) {
    super(root, data);
    this.listen(this.refs.remove, "click", this.removeItem);
  }

  removeItem() {
    const { value } = this.data;
    this.trigger("email-removed", { value });
  }

  get template() {
    const isValid = isValidEmail(this.data.value);
    return /*html*/ `
      <div class="email-input-item ${isValid ? "" : "email-input-item--invalid"}">
        <span data-ref="value">${this.data.value}</span>
        <button class="email-input-remove" data-ref="remove" title="Remove from list">${IconX}</button>
      </div>
    `;
  }

  get styles() {
    return /*css*/ `
      .email-input-item {
        display: inline-block;
        margin: 0 ${theme.space.tiny} ${theme.space.tiny} 0;
        font-size: 1rem;
        padding: ${theme.padding.small} ${theme.padding.normal};
        line-height: 16px;
        background-color: ${theme.color.backgroundLightBlue};
        border-radius: ${theme.radius.huge};
        max-width: calc(100% - ${theme.padding.normal});
        white-space: nowrap;
        overflow-x: auto;
        border-bottom: 1px solid transparent; /* so that text always vertically aligns with invalid items */
      }

      .email-input-item--invalid {
        background-color: transparent;
        padding-left: 0;
        padding-right: 0;
        margin: 0 4px 4px 6px;
        border-bottom: 1px dashed red;
        border-radius: 0;
      }

      .email-input-remove {
        font-family: monospace;
        font-weight: 600;
        border: 0;
        background-color: transparent;
        cursor: pointer;
        padding: 0;
        margin: 0 0 0 ${theme.space.tiny};
        outline: 0;
      }

      .email-input-remove:hover {
        color: ${theme.color.action}
      }
    `;
  }
}

export default EmailInput;
