import EmailInput from "./email-input.js";

export default function (assert) {
  const { addEmail, getEmails, getValidEmails, getCount, root, refs } = EmailInput.mount();

  // --------------------------------------------------------------------------
  // Initial state

  assert("Initial count is zero", () => getCount() === 0);
  assert("Initial emails array is empty", () => getEmails().length === 0);

  // --------------------------------------------------------------------------
  // Adding an e-mail

  addEmail("jose@gmail.com");

  assert("Email has been added to list", () => getEmails()[0] === "jose@gmail.com");
  assert("Count has been updated", () => getCount() === 1);

  assert("Email item has been appended to the DOM", () => {
    const firstValue = refs.items.childNodes[0].firstElementChild;
    return firstValue && firstValue.textContent === "jose@gmail.com";
  });

  assert("Email item is returned from getEmails()", () => getEmails()[0] === "jose@gmail.com");
  assert("Email item is returned from getValidEmails()", () => getValidEmails()[0] === "jose@gmail.com");

  // --------------------------------------------------------------------------
  // Ignoring duplicate values

  addEmail("jose@gmail.com");

  assert("Duplicate values are ignored", () => getCount() === 1);

  // --------------------------------------------------------------------------
  // Invalid e-mail handling

  const invalidEmail = "invalidzzz@";
  addEmail(invalidEmail);

  assert("Invalid email has been added to list", () => getEmails()[1] === invalidEmail);
  assert("Invalid email is not returned from getValidEmails()", () => {
    return getValidEmails().includes(invalidEmail) === false;
  });

  // --------------------------------------------------------------------------
  // Removing e-mail

  const removeButton = refs.items.querySelector(".email-input-remove");

  assert("Find remove button for first item", () => removeButton !== null);

  removeButton.dispatchEvent(new Event("click"));

  assert("Valid e-mail has been removed after click", () => getCount() === 0);
  assert("Invalid e-mail still in list", () => getEmails().includes(invalidEmail));

  assert("Valid e-mail removed from the DOM", () => {
    const firstValue = refs.items.childNodes[0].firstElementChild;
    return firstValue && firstValue.textContent !== "jose@gmail.com";
  });

  // assert("This is a failing test", () => ![] === 0);
  // assert("This is a broken test", () => undefinhed === defined);
}
