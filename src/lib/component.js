let uid = 0;
const qsa = (selector, root = document) => Array.prototype.slice.call(root.querySelectorAll(selector), 0);

/**
 * Initialize a template on the provided root. If one was not provided,
 * the component is mounted on a detached element which will become it's .root property
 */
function mount(root, template) {
  const temp = document.createElement("div");
  temp.innerHTML = template.trim();
  const templateRoot = temp.firstChild;
  if (root && root.parentNode) {
    root.parentNode.replaceChild(templateRoot, root);
  }
  return templateRoot;
}

export class Component {
  constructor(root, data) {
    this.id = this.constructor.name || ++uid;
    this.data = data;
    this.root = mount(root, this.template);
    this.refs = this.getRefs(this.root);
    this.renderStyles();
  }

  /** Collect all children with a data-ref attribute, returns object keyed by ref name */
  getRefs() {
    return qsa("[data-ref]", this.root).reduce((refs, el) => {
      return { ...refs, [el.dataset.ref]: el };
    }, {});
  }

  /** A CSS string with the styles for this component */
  get styles() {
    return null;
  }

  /** An HTML string template for this component */
  get template() {
    throw new Error("Missing template for component " + this.id);
  }

  /**
   * Attach event handlers to DOM elements.
   * (tracking listeners and cleaning up on unmount was removed for simplicity)
   */
  listen(element, eventName, handler) {
    const fn = handler.bind(this);
    element.addEventListener(eventName, fn, false);
  }

  /** Trigger a custom event for communication across components. */
  trigger(eventName, payload) {
    const event = new CustomEvent(eventName, { detail: payload, bubbles: true });
    this.root.dispatchEvent(event);
  }

  /**
   * Adds the component styles to the DOM in a <style> tag. They are de-duplicated based on
   * component names. This means interpolation is not recommended in the styles getter, as it
   * will never see any updates.
   */
  renderStyles() {
    if (!this.styles) return;
    const id = "css-" + this.id;
    if (document.getElementById(id) === null) {
      const style = document.createElement("style");
      style.innerHTML = this.styles;
      style.id = id;
      document.body.appendChild(style);
    }
  }

  /** Allows components to expose a custom API object for direct third-party integration. */
  get api() {
    return null;
  }

  /**
   * Initializes an instance of the component on the provided root element.
   * Should be used by consumers in place of instantiating the component class directly.
   */
  static mount(root, options) {
    const instance = new this(root, options);
    return { root: instance.root, refs: instance.refs, ...instance.api };
  }
}
