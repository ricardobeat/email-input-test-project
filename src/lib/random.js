const draw = (arr) => arr[Math.floor(Math.random() * arr.length)];

export const names = [
  "john",
  "alexander",
  "mike",
  "paul",
  "george",
  "ringo",
  "mattbellamy",
  "stevierayvaughan",
  "chriscornell",
  "ariana",
  "madonna",
  "freddie",
];

export const domains = [
  "miro.com",
  "random.org",
  "jmail.com",
  "thecave.club",
  "london.city",
  "london.co.uk",
  "pizzahut.org",
  "post.nl",
  "expensive.tld",
  "hotmail.com",
  "getnada.com",
  "email.com",
  "amazing.ly",
  "in.space",
  "facebook.com",
  "at.home",
  "work.remote",
  "its.late",
  "and.im",
  "runn.in",
  "out.of",
  "ideas.net",
];

export const email = () => `${draw(names)}@${draw(domains)}`;
