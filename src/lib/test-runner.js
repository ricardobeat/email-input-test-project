let i = 0;
let failures = 0;

function log(msg, type) {
  const line = document.createElement("pre");
  line.innerHTML = msg;
  line.className = type;
  line.style.transitionDelay = ++i * 50 + "ms";

  document.body.appendChild(line);
  setTimeout(() => (line.style.opacity = 1), 10);
}

function assert(msg, condition) {
  let passed = false;

  try {
    const result = condition();
    passed = result !== false && result != null;
  } catch (e) {
    console.error(e);
    log(e.stack || e, "error");
  }

  if (!passed) {
    failures += 1;
  }

  const status = passed ? "ok" : "fail";
  log([msg, "<b>" + status.toUpperCase() + "</b>"].join(" "), status);
}

export default function test(suite) {
  try {
    suite(assert);
  } catch (e) {
    log("Fatal: " + e, "error");
    throw e;
  }
  log(`\n${i} tests, ${failures} failed`);
}
