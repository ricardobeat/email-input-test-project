export default {
  type: {
    heading: {
      fontSize: "20px",
      lineHeight: 1.35,
    },
  },
  color: {
    text: "#050038",
    action: "#4262FF",
    active: "#3d51d4",
    actionContrast: "#FFF",
    backgroundWhite: "#FFF",
    backgroundLight: "#F8F8F7",
    backgroundLightBlue: "rgba(102, 153, 255, 0.2)",
    muted: "#C3C2CF",
  },
  shadow: {
    large: "0px 8px 20px rgba(0, 0, 0, 0.2);",
  },
  radius: {
    small: "4px",
    normal: "6px",
    large: "8px",
    huge: "16px",
  },
  padding: {
    smaller: "2px",
    small: "4px",
    normal: "8px",
    large: "16px",
  },
  space: {
    tiny: "4px",
    smaller: "8px",
    small: "12px",
    normal: "24px",
    large: "32px",
    larger: "48px",
  },
};
